package com.example.demo.entities;



import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String name;
    private String email;

    public User(){}

    public User(String name, String s) {
        this.name = name;
        this.email = s;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

//    public void setId(long id) {
//        this.id = id;
//    }

//    public void setName(String name) {
//        this.name = name;
//    }

    public String toString() {
        return "User{id=" + this.id + ", name=" + this.name + ", email=" + this.email + "}";
    }

    // standard constructors / setters / getters / toString
}